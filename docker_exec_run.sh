#! /bin/bash

if ( [ -z $( docker ps -q -a -f name=$2 ) ] )
then
    if [ "$( docker images -q $1 )" == "" ]
    then
        echo "Image does not exist"
    else
        docker run -d -v /etc/passwd:/etc/passwd:ro --name=$2 $1 /bin/bash -c "while true; do sleep 60; done"
        docker exec -it --user=$(id -u):$(id -g $(whoami)) $2 bash
    fi
else
    if ( [ $( docker container inspect -f '{{.State.Status}}' $2 ) != "running" ] )
    then
        docker start $2
    fi
    docker exec -it --user=$(id -u):$(id -g $(whoami)) $2 bash
fi